export const FETCH_PRODUCTS_BEGIN = "FETCH_PRODUCTS_BEGIN";
export const FETCH_PRODUCTS_SUCCESS =  "FETCH_PRODUCTS_SUCCESS";
export const FETCH_PRODUCTS_FAILURE =  "FETCH_PRODUCTS_FAILURE";
export const ADD_TO_CART="ADD_TO_CART";
export const INCR_ITEM="INCR_ITEM";
export const DEL_ITEM="DEL_ITEM";