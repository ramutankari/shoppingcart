import React, { Component } from 'react';
import '../stylesheets/header.css';
class Header extends Component {
   componentDidMount() {       
      }
   render() {
      return (
         <div id="header-container">
            <div id="first-header">
               <div className="first_header_content">
                  <ul>
                     <li><a href="#save_more_app">SAVE MORE ON APP</a></li>
                     <li><a href="#sell_on_lazada">SELL ON LAZADA</a></li>
                     <li><a href="#cus_care">CUSTOMER CARE</a></li>
                     <li><a href="#track_order">TRACK MT ORDER</a></li>
                     <li><a href="#acc">ACCOUNT</a></li>
                     <li><a href="#tukar">TUKAR BAHASA</a></li>
                  </ul>
               </div>
            </div>
            <div id="second-header">
               <div className="second-header-content">
                  <div className="logo">
                     <img src="//laz-img-cdn.alicdn.com/images/ims-web/TB1DAhhdAfb_uJjSsD4XXaqiFXa.png" 
                     alt="Online Shopping Lazada.com.my Logo" />
                  </div>
                  <div className="search">
                     <input type="text"/>
                     <button></button>
                  </div>
                  <div className="cart_img"><img src="https://www.clipartmax.com/png/small/130-1303615_shopping-cart-icons-amazon-shopping-cart-icon.png" 
                  alt="Shopping Cart,icons - Amazon Shopping Cart Icon @clipartmax.com"/></div>
               </div>
            </div>
            <div id="third-header">
               <div className="third-header-content">
               <ul>
               <li className="dropdown">
                     <a href="javascript:void(0)" className="dropbtn">Categores</a>
                     <div className="dropdown-content">
                        <a href="#">Link 1</a>
                        <a href="#">Link 2</a>
                        <a href="#">Link 3</a>
                     </div>
                  </li>
                  <li><a >LazMall</a></li>
                  <li><a >Global Collection</a></li>
                  <li><a >Top Up & eStore</a></li>
                  <li><a >Voucher</a></li>
               </ul>
               </div>
            </div>
         </div>
       );
    }
 }
 export default Header;