import React, { Component } from 'react';
import '../App.css';
import Header from '../components/Header';

class App extends Component {
  render() {
    return (
      <div className="App">
      <div className="header_sec">
      <Header/>
      </div>
       
       {this.props.children}
      
       
      </div>
    );
  }
}

export default App;
