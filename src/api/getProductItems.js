export default function getProductItems(callback) {
    fetch("http://localhost:3001/storeItems")
    .then(function(response) {
      return response.json();
    }).then(function(data) {
      callback(data);
    }).catch(function(err) {
      console.log('Error ', err);
    });
  }