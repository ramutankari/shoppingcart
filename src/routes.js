import React from 'react';
import {Route, IndexRoute } from 'react-router';
import App from './containers/App';
import ProductListContainer from './containers/ProductListContainer';
import ProductDetails from './components/ProductDetails';
import CartListContainer from './containers/CartListContainer';

export default (
  <Route path="/" component={App}>
    <Route path="productlist" component={ProductListContainer} />
    <IndexRoute component={ProductListContainer} />
    <Route path ="productdetails" component = {ProductDetails} />
    <Route path ="cart" component = {CartListContainer} />
   </Route> 
);

// function authenticate(nextState, replace, callback) {
//   auth.loggedIn().then(function(loggedIn) {
//       if (!loggedIn) {
//         replace({
//           pathname: '/login',
//           state: { nextPathname: nextState.location.pathname }
//         })
//       }
//       callback();
//   });
// }