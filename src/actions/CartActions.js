import * as types from '../constants/CartActionTypes';
import getProductItems from '../api/getProductItems';
function getAllTheProducts( dispatch) {

    console.log('14')
    getProductItems( (data) => {
      dispatch({
        type: types.FETCH_PRODUCTS_SUCCESS,
        payload: data,
  
      });
    });
  }
  export function getcartproducts() {      
    return (dispatch) => {
      getAllTheProducts( dispatch);
    };
}
export function addToCart(item) {
  return (dispatch, getState) => {
    console.log(getState(),"getState");
     let selectedAr = getState().cartReducer.selecteditems
    item.selectedCount = 1
   selectedAr.push(item);
    dispatch({
      type: types.ADD_TO_CART,
      selected: selectedAr,

    });
  }
}
export function incrementItems(index, type) {
  return (dispatch, getState) => {
    let selectedAr = getState().cartReducer.selecteditems
    if(type == 'inc') {
      selectedAr[index].selectedCount += 1;
    }else{
      selectedAr[index].selectedCount -= 1;
    }
    dispatch({
      type: types.INCR_ITEM,
      selected: selectedAr,

    });
  }
}
export function deletecartItem(i){
  return (dispatch, getState) => {
    let selectedAr = getState().cartReducer.selecteditems
    selectedAr.splice(i,1);
    dispatch({
      type: types.DEL_ITEM,
      selected: selectedAr,

    });
  }
}
// export function getAllCartProducts(){
//   return (dispatch)
// }
