import React, { Component } from 'react';
import {browserHistory} from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as action from '../actions/CartActions';
import '../stylesheets/productdetails.css';
class ProductDetails extends Component {
    gotoCart(item){
        this.props.actions.addToCart(item)
        browserHistory.push('/cart');
    }
    render() {
        console.log("details", this.props.location.state.data);
        const itemDetails = this.props.location.state.data;
        return (
            <div>
                <div className="product-details">
                    <div className="left">
                        <img src={itemDetails.imgSrc} width="100%" height="300px" />
                    </div>
                    <div className="right">
                        <h2 className="">{itemDetails.itemName}</h2>
                        <hr/>
                        <h3 className="cost">${itemDetails.price}</h3> 
                        <p>{itemDetails.quantityRemaining} in stock</p>
                        <button onClick={() => this.gotoCart(itemDetails)} className="add_cart_but">Add to cart</button>
                    </div>
                </div>
            </div>
        )
    }
}
const mapStateToProps = state => ({
    products: state.cartReducer,
   
 });
 function mapDispatchToProps(dispatch) {
   return {actions: bindActionCreators(action,dispatch)}
 }
 
export default connect(mapStateToProps,mapDispatchToProps)(ProductDetails);