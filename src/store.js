import { createStore, combineReducers, applyMiddleware } from 'redux';
import cartReducer from './reducers/CartReducer';
import thunk from 'redux-thunk'
const reducer = combineReducers({
 cartReducer
})
const store = createStore(
 reducer,
 applyMiddleware(thunk)
)
export default store;