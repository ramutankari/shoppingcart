import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as action from '../actions/CartActions';
import ProductList from '../components/ProductList';

class ProductListContainer extends Component {
componentDidMount() {
   
      this.props.actions.getcartproducts();
   
}
  render() {
const { products } = this.props;      
  
  if(this.props.ProductItems && this.props.ProductItems.length === 0){
    console.log('no data');
    return(
        <div>
            <div>no data</div>
        </div>
    )
  }
  else if(this.props.products && this.props.products.ProductItems.length > 0){
    console.log('data');
    return (
        <div>
          <div className="">
            <div className="">
            <ProductList  ProductItems={products.ProductItems} />
              </div>          
          </div>
        </div>
      );
  }
  else{
      return(
          <div>
              <div>loading...</div>
          </div>
      )
  }
   
  }
}
const mapStateToProps = state => ({
     products: state.cartReducer,
    // loading: state.products.loading,
     error: state
  });
  function mapDispatchToProps(dispatch) {
    return {actions: bindActionCreators(action,dispatch)}
  }
  
  export default connect(mapStateToProps,mapDispatchToProps)(ProductListContainer);