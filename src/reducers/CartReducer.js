import * as types from '../constants/CartActionTypes';

const initialState = {
  ProductItems: [],
  loading: false,
  error: null,
  selecteditems: [],
};

export default function cartReducer(state = initialState, action) {
  console.log("action", action.type);
  switch (action.type) {
    case types.FETCH_PRODUCTS_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      };

    case types.FETCH_PRODUCTS_SUCCESS:
      return {
        ...state,
        ProductItems: action.payload
      }
    case types.ADD_TO_CART:
      console.log(action, state)
      return {
        ...state,
        selecteditems: [...action.selected]
      };

    case types.INCR_ITEM:
      console.log(action)
      return {
        ...state,
        selecteditems: [...action.selected]
      };
    case types.DEL_ITEM:
      console.log(action)
      return {
        ...state,
        selecteditems: [...action.selected]
      };

    default:
      return state;
  }
}
