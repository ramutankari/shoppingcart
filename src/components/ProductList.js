import React, { Component } from 'react';
import {browserHistory} from 'react-router';
import '../stylesheets/productlist.css'
export default class ProductList extends Component {  

    selectItem(item) {        
        browserHistory.push({
          pathname: '/productdetails',
          
          state: { data: item }
        })
      
    }

  render() {
      console.log(this.context,'render')
      
    return (
      <div>
        <div className="product-list-container">
          <div>
            <h3>Just For You</h3>
          </div>
          <div className='lists'>
          {
           
              this.props.ProductItems.map((item, index) => {
                return (
                  <div className="product-list" key={`PhotoItem_${item.id}_${index}`}>
                      <div className="image-item">
                    <img src={item.imgSrc}  width="100%" height="300px" onClick={()=>this.selectItem(item)}/>
                          <h4 className="pull-left">{item.itemName}</h4>
                          <h5 className="pull-right">${item.price}</h5> <h6>{item.quantityRemaining} in stock</h6>
                          {/* <button onClick={()=>this.selectItem(item)}  className="btn btn-success">Add to cart</button> */}
                         
                          </div>
                  </div>
                );
              })
              
          }
          </div>
          <div className="clearfix" />
        </div>


      </div>
    );
  }
}
