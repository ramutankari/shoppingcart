import React, { Component } from 'react';
import { Link } from 'react-router';
import '../stylesheets/cartlist.css';
class CartList extends Component {   
    
    incrementItem(i, type, item) {
        if (type === 'dec' && item.selectedCount === 1) {
            return;
        }
        this.props.actions.incrementItems(i, type);
    }
    deletecartItem(index, item) {
        this.props.actions.deletecartItem(index);
    }
    renderDiv() {
        return (
            <div>
                <div className="card_deails_sec">
                            <div>
                                <h2 className="card_details">Card Details</h2>
                            </div>
                            <div>
                                <h5>Select Card Type</h5>
                                <ul className="card_type">
                                    <li>credit</li>
                                    <li>Debit</li>
                                    <li>visa</li>

                                </ul>

                            </div>
                            <div class="card_num">
                                <h5>Card Number</h5>
                                <input type="text" />

                            </div>
                            <div className="card_sec">
                                <div className="exp_date"> 
                                    <p>Expiry date</p>
                                    <div>
                                    <span>
                                        <input type="text" />
                                        <input type="text" />
                                        /
                                        <input type="text" />
                                        <input type="text" />
                                        /
                                        <input type="text" />
                                        <input type="text" />
                                    </span>
                                    </div>
                                    
                                </div>
                                <div className="cvv">
                                    <p>CVV</p>
                                    <input type="text" />
                                </div>
                            </div>
                           
                        </div>
                         <div className="check_out">
                         Check Out
                     </div>
                     </div>
        )
    }
    render() {
        const { cartproducts } = this.props;       
        if (cartproducts && cartproducts.length > 0) {
            return (
                <div>
                    <Link to="/productlist">GoTo ProductList</Link>

                    <div className="cart_list_container">
                        <div className="cart_left">
                            <ul className="cart_list">
                                {
                                    cartproducts.map((item, index) => {
                                        return (
                                            <li className="cart-list" key={`PhotoItem_${item.id}_${index}`}>
                                                <ul className="cart_list_details">
                                                    <li className="image-item">
                                                        <img src={item.imgSrc} width="50px" height="50px" alt="cart item"/>
                                                    </li>
                                                    <li>
                                                        <div className="cart-data-div">
                                                            <span className="pull-left">{item.itemName.slice(0, 6)}
                                                            </span>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div className="cart-data-div"><span>blue</span></div></li>
                                                    <li><div className="cart-data-div">
                                                    <span>{item.selectedCount}</span></div></li>
                                                    <li>
                                                        <p className="item-count" onClick={() => this.incrementItem(index, 'inc', item)}><span>+</span></p>
                                                        <p className="item-count" onClick={() => this.incrementItem(index, 'dec', item)}><span>-</span></p>
                                                    </li>
                                                    <li><div className="cart-data-div"><span>some text</span></div></li>
                                                    <li onClick={() => this.deletecartItem(index, item)}>
                                                    <div className="cart-data-div"><span>X</span></div></li>
                                                </ul>

                                            </li>
                                        );
                                    })
                                }
                            </ul>
                        </div>
                        <div className="cart_right">{this.renderDiv()}</div>


                        
                    </div>
                </div>
            )
        }
        else if (cartproducts && cartproducts.length === 0) {
            return (
                <div>
                    <div className="cart_left">
                    <Link to="/productlist">GoTo ProductList</Link>
                    <div>No Items in Cart</div>
                    </div>
                    <div className="cart_right">{this.renderDiv()}</div>
                </div>
            )
        }
        else {
            return (
                <div className="cart_left">
                   
                    <Link to="/productlist">GoTo ProductList</Link>
                    <div className="loading">Loading...</div>
                </div>
            )
        }

    }
}

export default CartList;