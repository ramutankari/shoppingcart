import React, { Component } from 'react';
import { Link } from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as action from '../actions/CartActions';
import CartList from '../components/CartList';
import '../stylesheets/cartlist.css';
class CartListContainer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { cartproducts, actions } = this.props;
        
        return (
            <div>
                <CartList actions={actions} cartproducts={cartproducts} />
            </div>
        )
    }


}
const mapStateToProps = state => ({
    cartproducts: state.cartReducer.selecteditems,

});
function mapDispatchToProps(dispatch) {
    return { actions: bindActionCreators(action, dispatch) }
}
export default connect(mapStateToProps, mapDispatchToProps)(CartListContainer);